# README #

### Original Unity Amplitude Plugin

[Link](https://github.com/amplitude/unity-plugin)

### Amplitude Initialization

	private void InitAmplitude()
	{
		Amplitude amplitude = Amplitude.Instance;
		amplitude.trackSessionEvents(true);
		amplitude.useAdvertisingIdForDeviceId();

	#if UNITY_EDITOR
    	amplitude.logging = true;
	#endif

		amplitude.init(YOUR API KEY);
	}
